def loja():
	a, b = 0, 0
	while True:
		try:
			x = int(input("Digite 1 caso deseje inserir, e 2 caso deseje finalizar: ")) 
			if x == 2:
				break
			if x != 1:
				raise TypeError
			y = float(input("Digite o valor do produto:"))
			z = float(input("Digite o desconto do produto de 0 a 100: "))
			if y < 0 or z < 0:
				raise NameError
			a += y
			b += y * ((100-z)/100)
		except ValueError:
			print("Digite um valor numérico.\n")
		except TypeError:
			print("Digite somente 1 ou 2.\n")
		except NameError:
			print("Números negativos são invalidos.\n")
	return a, b

a, b = loja()
print("Você pagaria:", a, "\nVocê vai pagar:", b)