Bandeiras em graphics.py:

Armenia
from graphics import *

largura=float(1000)
comprimento=float(500)

win = GraphWin("Flag of Armenia!", largura, comprimento)
win.setBackground(color_rgb(242, 168, 0))

retangulo1 = Rectangle(Point(0, 0), Point(largura, comprimento/3))
retangulo1.setFill(color_rgb(217, 0, 18))
retangulo1.setOutline(color_rgb(217, 0, 18))
retangulo1.draw(win)

retangulo2 = Rectangle(Point(0, comprimento/3), Point(largura, comprimento*2/3))
retangulo2.setFill(color_rgb(0, 51, 160))
retangulo2.setOutline(color_rgb(0, 51, 160))
retangulo2.draw(win)


win.getMouse()
win.close()


Brazil
from graphics import *

#Retângulo

win = GraphWin("Flag of Brazil!", 1000, 700)
win.setBackground(color_rgb(0, 156, 59))

#Losangulo

Losang = Polygon(Point(500, 85), Point(915, 350), Point(500, 615), Point(85, 350))
Losang.setFill(color_rgb(255, 223, 0))
Losang.setOutline(color_rgb(255, 223, 0))
Losang.draw(win)

#Círculo

circ = Circle(Point(500, 350), 175)
circ.setFill(color_rgb(0, 39, 118))
circ.setOutline(color_rgb(0, 39, 118))
circ.draw(win)

#Faixa
faixa = Circle(Point(400, 699),410)
faixa.setOutline(color_rgb(255,255,255))
faixa.setWidth(28)
faixa.draw(win)

#Correções

corr1 = Circle(Point(500,350), 195)
corr1.setOutline(color_rgb(255, 223, 0))
corr1.setWidth(40)
corr1.draw(win)

corr2 = Polygon(Point(85, 350), Point(315, 250), Point(315, 497))
corr2.setFill(color_rgb(255, 223, 0))
corr2.setOutline(color_rgb(255, 223, 0))
corr2.draw(win)

corr3 = Polygon(Point(915, 350), Point(695, 250), Point(695, 490))
corr3.setFill(color_rgb(255, 223, 0))
corr3.setOutline(color_rgb(255, 223, 0))
corr3.draw(win)

cr3 = Polygon(Point(85, 351), Point(500, 616), Point(0,700), Point(0, 350))
cr3.setFill(color_rgb(0, 156, 59))
cr3.setOutline(color_rgb(0, 156, 59))
cr3.draw(win)

cr4 = Polygon(Point(500, 616), Point(915, 351), Point(916,350), Point(1000, 700), Point(501,700))
cr4.setFill(color_rgb(0, 156, 59))
cr4.setOutline(color_rgb(0, 156, 59))
cr4.draw(win)

#Texto

O = Text(Point(355, 292), "O")
O.setOutline(color_rgb(0, 156, 59))
O.setSize(22)
O.draw(win)

R = Text(Point(378, 290), "R")
R.setOutline(color_rgb(0, 156, 59))
R.setSize(22)
R.draw(win)

D = Text(Point(400, 290), "D")
D.setOutline(color_rgb(0, 156, 59))
D.setSize(22)
D.draw(win)

E = Text(Point(420, 291), "E")
E.setOutline(color_rgb(0, 156, 59))
E.setSize(22)
E.draw(win)

M = Text(Point(443, 292), "M")
M.setOutline(color_rgb(0, 156, 59))
M.setSize(22)
M.draw(win)

e = Text(Point(480, 298), "E")
e.setOutline(color_rgb(0, 156, 59))
e.setSize(19)
e.draw(win)

P = Text(Point(510, 304), "P")
P.setOutline(color_rgb(0, 156, 59))
P.setSize(22)
P.draw(win)

r = Text(Point(530, 310), "R")
r.setOutline(color_rgb(0, 156, 59))
r.setSize(22)
r.draw(win)

o = Text(Point(551, 318), "O")
o.setOutline(color_rgb(0, 156, 59))
o.setSize(22)
o.draw(win)


G = Text(Point(572, 327), "G")
G.setOutline(color_rgb(0, 156, 59))
G.setSize(22)
G.draw(win)

rr = Text(Point(592, 336), "R")
rr.setOutline(color_rgb(0, 156, 59))
rr.setSize(22)
rr.draw(win)

ee = Text(Point(610, 347), "E")
ee.setOutline(color_rgb(0, 156, 59))
ee.setSize(22)
ee.draw(win)

S = Text(Point(627, 358), "S")
S.setOutline(color_rgb(0, 156, 59))
S.setSize(22)
S.draw(win)

s = Text(Point(644, 370), "S")
s.setOutline(color_rgb(0, 156, 59))
s.setSize(22)
s.draw(win)

oo = Text(Point(661, 383), "O")
oo.setOutline(color_rgb(0, 156, 59))
oo.setSize(22)
oo.draw(win)

# Estrela - Funçao

def estrela(pontoM, lado, corFundo, Outline, janela):

#Ponto M
 pM = Point(pontoM.getX(), pontoM.getY())

#ponto A
 pA = Point(pontoM.getX()-lado*0.5, pontoM.getY()+lado*0.69)

#ponto B
 pB = Point(pontoM.getX()+lado*0.5, pontoM.getY()+lado*0.69)

#ponto C
 pC = Point(pontoM.getX()+lado*0.81, pontoM.getY()-lado*0.26)

#ponto D
 pD = Point(pontoM.getX(), pontoM.getY()-lado*0.85)

#ponto E
 pE = Point(pontoM.getX()-lado*0.81, pontoM.getY()-lado*0.26)

 p = Polygon(pA, pC, pE, pB, pD)
 p.setOutline(Outline)
 p.setFill(corFundo)
 p.draw(janela)

# Estrelas - Posiçoes

E1=estrela(Point(702/2, 816/2), round(4/5*7),"white", "white", win)

E2=estrela(Point(714/2, 636/2), 7,"white", "white", win)

E3=estrela(Point(746/2, 784/2), 7,"white", "white", win)

E4=estrela(Point(780/2, 762/2), round(2/5*7),"white", "white", win)

E5=estrela(Point(808/2, 854/2), round(3/5*7),"white", "white", win)

E6=estrela(Point(818/2, 819/2), round(4/5*7),"white", "white", win)

E7=estrela(Point(860/2, 886/2), 7,"white", "white", win)

E8=estrela(Point(868/2, 714/2), round(4/5*7),"white", "white", win)

E9=estrela(Point(966/2, 788/2), round(3/5*7),"white", "white", win)

E10=estrela(Point(982/2, 812/2), round(2/5*7),"white", "white", win)

E11=estrela(Point(1000/2, 856/2), 7,"white", "white", win)

E12=estrela(Point(1000/2, 974/2), round(1/5*7),"white", "white", win)

E13=estrela(Point(1000/2, 756/2), round(4/5*7),"white", "white", win)

E14=estrela(Point(1040/2, 788/2), round(4/5*7),"white", "white", win)

E15=estrela(Point(1078/2, 896/2), round(3/5*7),"white", "white", win)

E16=estrela(Point(1095/2, 682/2), round(3/5*7),"white", "white", win)

E17=estrela(Point(1108/2, 590/2), 7,"white", "white", win)

E18=estrela(Point(1104/2, 936/2), round(4/5*7),"white", "white", win)

E19=estrela(Point(1135/2, 904/2), round(3/5*7),"white", "white", win)

E20=estrela(Point(1173/2, 893/2), round(3/5*7),"white", "white", win)

E21=estrela(Point(1175/2, 927/2), round(4/5*7),"white", "white", win)

E22=estrela(Point(1174/2, 962/2), round(3/5*7),"white", "white", win)

E23=estrela(Point(1209/2, 899/2), round(3/5*7),"white", "white", win)

E24=estrela(Point(1237/2, 882/2), round(4/5*7),"white", "white", win)

E25=estrela(Point(1245/2, 823/2), 7,"white", "white", win)

E26=estrela(Point(1259/2, 854/2), round(4/5*7),"white", "white", win)

E27=estrela(Point(1293/2, 826/2), round(3/5*7),"white", "white", win)




# Aguardando um clique de mouse para fechar a janela
win.getMouse()
win.close()





Chile
from graphics import *
from math import *


win = GraphWin("Flag of Chile!", 900, 600)
win.setBackground(color_rgb(255,255,255))

retangulo1 = Rectangle(Point(0, 0), Point(300, 300))
retangulo1.setFill(color_rgb(0,57,166))
retangulo1.setOutline(color_rgb(0,57,166))
retangulo1.draw(win)

retangulo2 = Rectangle(Point(0, 300), Point(900, 600))
retangulo2.setFill(color_rgb(213,43,30))
retangulo2.setOutline(color_rgb(213,43,30))
retangulo2.draw(win)

def estrela(pontoM, lado, a, corFundo, Outline, janela):


 #ponto A
 pA = Point(pontoM.getX() + lado*cos(radians(a+90)), pontoM.getY() - lado*sin(radians(a+90)))


 #ponto B
 pB = Point(pontoM.getX() + lado*cos(radians(a+162)), pontoM.getY() - lado*sin(radians(a+162)))


 #ponto C
 pC = Point(pontoM.getX() + lado*cos(radians(a+234)), pontoM.getY() - lado*sin(radians(a+234)))

 #ponto D
 pD = Point(pontoM.getX() + lado*cos(radians(a+306)), pontoM.getY() - lado*sin(radians(a+306)))

 #ponto E
 pE = Point(pontoM.getX() + lado*cos(radians(a+18)), pontoM.getY() - lado*sin(radians(a+18)))

 p = Polygon(pA, pC, pE, pB, pD)
 p.setOutline(Outline)
 p.setFill(corFundo)
 p.draw(janela)

# Estrelas - Posiçoes

E1=estrela(Point(150, 150), 75, 0, color_rgb(255,255,255), color_rgb(255,255,255), win)



win.getMouse()
win.close()





Israel
from graphics import *
from math import *


win = GraphWin("Flag of Israel!", 880, 640)
win.setBackground("white")

def estrela(pontoM, raio, a, corFundo, Outline, janela):

  #ponto A
  pA = Point(pontoM.getX() + raio*cos(radians(a+90)), pontoM.getY() - raio*sin(radians(a+90)))

  #ponto B
  pB = Point(pontoM.getX() + raio*cos(radians(a+150)), pontoM.getY() - raio*sin(radians(a+150)))

  #ponto C
  pC = Point(pontoM.getX() + raio*cos(radians(a+210)), pontoM.getY() - raio*sin(radians(a+210)))

  #ponto D
  pD = Point(pontoM.getX() + raio*cos(radians(a+270)), pontoM.getY() - raio*sin(radians(a+270)))

  #ponto E
  pE = Point(pontoM.getX() + raio*cos(radians(a+330)), pontoM.getY() - raio*sin(radians(a+330)))

  #ponto F
  pF = Point(pontoM.getX() + raio*cos(radians(a+30)), pontoM.getY() - raio*sin(radians(a+30)))


  #hexagono menor
  #ponto a
  pa = Point(pontoM.getX() + (raio*2/3/1.75)*cos(radians(a+60)), pontoM.getY() - (raio*2/3/1.75)*sin(radians(a+60)))

  #ponto b
  pb = Point(pontoM.getX() + (raio*2/3/1.75)*cos(radians(a+120)), pontoM.getY() - (raio*2/3/1.75)*sin(radians(a+120)))

  #ponto c
  pc = Point(pontoM.getX() + (raio*2/3/1.75)*cos(radians(a+180)), pontoM.getY() - (raio*2/3/1.75)*sin(radians(a+180)))

  #ponto d
  pd = Point(pontoM.getX() + (raio*2/3/1.75)*cos(radians(a+240)), pontoM.getY() - (raio*2/3/1.75)*sin(radians(a+240)))

  #ponto e
  pe = Point(pontoM.getX() + (raio*2/3/1.75)*cos(radians(a+300)), pontoM.getY() - (raio*2/3/1.75)*sin(radians(a+300)))

  #ponto f
  pf = Point(pontoM.getX() + (raio*2/3/1.75)*cos(radians(a+0)), pontoM.getY() - (raio*2/3/1.75)*sin(radians(a+0)))

  #triangulos menores

  pA1 = Point(pontoM.getX() + (raio*0.71)*cos(radians(a+90)), pontoM.getY() - (raio*0.71)*sin(radians(a+90)))
  pA2 = Point(pontoM.getX() + raio*0.55* cos(radians(a + 80)), pontoM.getY() - raio*0.55* sin(radians(a + 80)))
  pA3 = Point(pontoM.getX() + raio * 0.55 * cos(radians(a + 100)), pontoM.getY() - raio * 0.55 * sin(radians(a + 100)))

  pB1 = Point(pontoM.getX() + (raio * 0.71) * cos(radians(a + 150)), pontoM.getY() - (raio*0.71)*sin(radians(a+150)))
  pB2 = Point(pontoM.getX() + raio*0.55* cos(radians(a + 140)), pontoM.getY() - raio*0.55* sin(radians(a + 140)))
  pB3 = Point(pontoM.getX() + raio * 0.55 * cos(radians(a + 160)), pontoM.getY() - raio * 0.55 * sin(radians(a + 160)))

  pC1 = Point(pontoM.getX() + (raio * 0.71) * cos(radians(a+210)), pontoM.getY()-(raio*0.71)*sin(radians(a+210)))
  pC2 = Point(pontoM.getX() + raio*0.55* cos(radians(a + 200)), pontoM.getY() - raio*0.55* sin(radians(a + 200)))
  pC3 = Point(pontoM.getX() + raio * 0.55 * cos(radians(a + 220)), pontoM.getY() - raio * 0.55 * sin(radians(a + 220)))

  pD1 = Point(pontoM.getX() + (raio*0.71)*cos(radians(a+270)), pontoM.getY() - (raio*0.71)*sin(radians(a+270)))
  pD2 = Point(pontoM.getX() + raio*0.55* cos(radians(a + 260)), pontoM.getY() - raio*0.55* sin(radians(a + 260)))
  pD3 = Point(pontoM.getX() + raio * 0.55 * cos(radians(a + 280)), pontoM.getY() - raio * 0.55 * sin(radians(a + 280)))

  pE1 = Point(pontoM.getX() + (raio*0.71)*cos(radians(a+330)), pontoM.getY() - (raio*0.71)*sin(radians(a+330)))
  pE2 = Point(pontoM.getX() + raio*0.55* cos(radians(a + 320)), pontoM.getY() - raio*0.55* sin(radians(a + 320)))
  pE3 = Point(pontoM.getX() + raio * 0.55 * cos(radians(a + 340)), pontoM.getY() - raio * 0.55 * sin(radians(a + 340)))

  pF1 = Point(pontoM.getX() + (raio*0.71)*cos(radians(a+30)), pontoM.getY() - (raio*0.71)*sin(radians(a+30)))
  pF2 = Point(pontoM.getX() + raio*0.55* cos(radians(a + 20)), pontoM.getY() - raio*0.55* sin(radians(a + 20)))
  pF3 = Point(pontoM.getX() + raio * 0.55 * cos(radians(a + 40)), pontoM.getY() - raio * 0.55 * sin(radians(a + 40)))


  p1 = Polygon(pA, pC, pE)
  p1.setOutline(Outline)
  p1.setFill(Outline)
  p1.draw(janela)

  p2 = Polygon(pB, pD, pF)
  p2.setOutline(Outline)
  p2.setFill(Outline)
  p2.draw(janela)

  #hexagono menor
  p3 = Polygon(pa, pb, pc, pd, pe, pf)
  p3.setOutline(corFundo)
  p3.setFill(corFundo)
  p3.draw(janela)

  #triangulos menores
  t1 = Polygon(pA1, pA2, pA3)
  t1.setOutline(corFundo)
  t1.setFill(corFundo)
  t1.draw(janela)

  t2 = Polygon(pB1, pB2, pB3)
  t2.setOutline(corFundo)
  t2.setFill(corFundo)
  t2.draw(janela)

  t3 = Polygon(pC1, pC2, pC3)
  t3.setOutline(corFundo)
  t3.setFill(corFundo)
  t3.draw(janela)

  t4 = Polygon(pD1, pD2, pD3)
  t4.setOutline(corFundo)
  t4.setFill(corFundo)
  t4.draw(janela)

  t5 = Polygon(pE1, pE2, pE3)
  t5.setOutline(corFundo)
  t5.setFill(corFundo)
  t5.draw(janela)

  t6 = Polygon(pF1, pF2, pF3)
  t6.setOutline(corFundo)
  t6.setFill(corFundo)
  t6.draw(janela)

# Estrelas - Posiçoes

E1=estrela(Point(440, 320), 132, 0, "white", color_rgb(0, 56, 184), win)

faixa1 = Rectangle(Point(0, 60), Point(880, 160))
faixa1.setFill(color_rgb(0, 56, 184))
faixa1.setOutline(color_rgb(0, 56, 184))
faixa1.draw(win)

faixa2 = Rectangle(Point(0, 480), Point(880, 580))
faixa2.setFill(color_rgb(0, 56, 184))
faixa2.setOutline(color_rgb(0, 56, 184))
faixa2.draw(win)

win.getMouse()
win.close()





Myanmar
from graphics import *

largura=float(2500)
comprimento=float(20)
win = GraphWin("Flag of Myanmar!", largura, comprimento)
win.setBackground(color_rgb(254, 203, 0))

retangulo1 = Rectangle(Point(0, comprimento/3), Point(largura, comprimento*13/20))
retangulo1.setFill(color_rgb(52, 178, 51))
retangulo1.setOutline(color_rgb(52, 178, 51))
retangulo1.draw(win)

retangulo2 = Rectangle(Point(0, comprimento*13/20), Point(largura, comprimento))
retangulo2.setFill(color_rgb(234, 40, 57))
retangulo2.setOutline(color_rgb(234, 40, 57))
retangulo2.draw(win)

raiodef=()
if largura > comprimento:
   raiodef=comprimento*5/12
else:
   raiodef=largura*5/12

def estrela(pontoM, lado, corFundo, Outline, janela):

#Ponto M
pM = Point(pontoM.getX(), pontoM.getY())

#ponto A
pA = Point(pontoM.getX()-lado*0.5, pontoM.getY()+lado*0.69)

#ponto B
pB = Point(pontoM.getX()+lado*0.5, pontoM.getY()+lado*0.69)

#ponto C
pC = Point(pontoM.getX()+lado*0.81, pontoM.getY()-lado*0.26)

#ponto D
pD = Point(pontoM.getX(), pontoM.getY()-lado*0.85)

#ponto E
pE = Point(pontoM.getX()-lado*0.81, pontoM.getY()-lado*0.26)

p = Polygon(pA, pC, pE, pB, pD)
p.setOutline(Outline)
p.setFill(corFundo)
p.draw(janela)

# Estrelas - Posiçoes

E1=estrela(Point(largura/2, comprimento*31/60), raiodef,"white", "white", win)



win.getMouse()
win.close()


North Korea
from graphics import *
from math import *


win = GraphWin("Flag of North Korea!", 720, 360)
win.setBackground(color_rgb(2,79,162))

retangulo1 = Rectangle(Point(0, 60), Point(720, 70))
retangulo1.setFill("white")
retangulo1.setOutline("white")
retangulo1.draw(win)

retangulo2 = Rectangle(Point(0, 290), Point(720, 300))
retangulo2.setFill("white")
retangulo2.setOutline("white")
retangulo2.draw(win)

retangulo3 = Rectangle(Point(0, 70), Point(720, 290))
retangulo3.setFill(color_rgb(237,28,39))
retangulo3.setOutline(color_rgb(237,28,39))
retangulo3.draw(win)

def estrela(pontoM, lado, a, corFundo, Outline, janela):


  #ponto A
  pA = Point(pontoM.getX() + lado*cos(radians(a+90)), pontoM.getY() - lado*sin(radians(a+90)))


  #ponto B
  pB = Point(pontoM.getX() + lado*cos(radians(a+162)), pontoM.getY() - lado*sin(radians(a+162)))


  #ponto C
  pC = Point(pontoM.getX() + lado*cos(radians(a+234)), pontoM.getY() - lado*sin(radians(a+234)))

  #ponto D
  pD = Point(pontoM.getX() + lado*cos(radians(a+306)), pontoM.getY() - lado*sin(radians(a+306)))

  #ponto E
  pE = Point(pontoM.getX() + lado*cos(radians(a+18)), pontoM.getY() - lado*sin(radians(a+18)))

  p = Polygon(pA, pC, pE, pB, pD)
  p.setOutline(Outline)
  p.setFill(corFundo)
  p.draw(janela)

circulo = Circle(Point(240,180), 80)
circulo.setOutline("white")
circulo.setFill("white")
circulo.draw(win)

# Estrelas - Posiçoes

E1=estrela(Point(240, 180), 77.5, 0, color_rgb(237,28,39), color_rgb(237,28,39), win)



win.getMouse()
win.close()

Panama
from graphics import *

#Retângulo

win = GraphWin("Flag of Panama!", 900, 600)
win.setBackground("white")

# Desenhando Retângulos
r1 = Rectangle(Point(0, 300), Point(450, 600))
r1.setFill(color_rgb(0, 82, 147))
r1.setOutline(color_rgb(0, 82, 147))
r1.draw(win)

r2 = Rectangle(Point(450, 0), Point(900, 300))
r2.setFill(color_rgb(210, 16, 52))
r2.setOutline(color_rgb(210, 16, 52))
r2.draw(win)

# Estrela - Funçao

def estrela(pontoM, lado, corFundo, Outline, janela):

#Ponto M
pM = Point(pontoM.getX(), pontoM.getY())

#ponto A
pA = Point(pontoM.getX()-lado*0.5, pontoM.getY()+lado*0.69)

#ponto B
pB = Point(pontoM.getX()+lado*0.5, pontoM.getY()+lado*0.69)

#ponto C
pC = Point(pontoM.getX()+lado*0.81, pontoM.getY()-lado*0.26)

#ponto D
pD = Point(pontoM.getX(), pontoM.getY()-lado*0.85)

#ponto E
pE = Point(pontoM.getX()-lado*0.81, pontoM.getY()-lado*0.26)

p = Polygon(pA, pC, pE, pB, pD)
p.setOutline(Outline)
p.setFill(corFundo)
p.draw(janela)

# Estrelas - Posiçoes

E1=estrela(Point(225, 150), 70,(color_rgb(0, 82, 147)), (color_rgb(0, 82, 147)), win)

E2=estrela(Point(675, 450), 70, color_rgb(210, 16, 52), color_rgb(210, 16, 52), win)



win.getMouse()
win.close()





Somalia
from graphics import *
from math import *


win = GraphWin("Flag of Somalia!", 900, 600)
win.setBackground(color_rgb(65, 137, 221))

def estrela(pontoM, lado, a, corFundo, Outline, janela):


   #ponto A
   pA = Point(pontoM.getX() + lado*cos(radians(a+90)), pontoM.getY() - lado*sin(radians(a+90)))


   #ponto B
   pB = Point(pontoM.getX() + lado*cos(radians(a+162)), pontoM.getY() - lado*sin(radians(a+162)))


   #ponto C
   pC = Point(pontoM.getX() + lado*cos(radians(a+234)), pontoM.getY() - lado*sin(radians(a+234)))

   #ponto D
   pD = Point(pontoM.getX() + lado*cos(radians(a+306)), pontoM.getY() - lado*sin(radians(a+306)))

   #ponto E
   pE = Point(pontoM.getX() + lado*cos(radians(a+18)), pontoM.getY() - lado*sin(radians(a+18)))

   p = Polygon(pA, pC, pE, pB, pD)
   p.setOutline(Outline)
   p.setFill(corFundo)
   p.draw(janela)

# Estrelas - Posiçoes

E1=estrela(Point(450, 300), 144.4, 0, color_rgb(255, 255, 255), color_rgb(255, 255, 255), win)



win.getMouse()
win.close()




Vietnam
from graphics import *

win = GraphWin("Flag of Vietnam!", 900, 600)
win.setBackground(color_rgb(218, 37, 29))

def estrela(pontoM, lado, corFundo, Outline, janela):

#Ponto M
pM = Point(pontoM.getX(), pontoM.getY())

#ponto A
pA = Point(pontoM.getX()-lado*0.5, pontoM.getY()+lado*0.69)

#ponto B
pB = Point(pontoM.getX()+lado*0.5, pontoM.getY()+lado*0.69)

#ponto C
pC = Point(pontoM.getX()+lado*0.81, pontoM.getY()-lado*0.26)

#ponto D
pD = Point(pontoM.getX(), pontoM.getY()-lado*0.85)

#ponto E
pE = Point(pontoM.getX()-lado*0.81, pontoM.getY()-lado*0.26)

p = Polygon(pA, pC, pE, pB, pD)
p.setOutline(Outline)
p.setFill(corFundo)
p.draw(janela)

# Estrelas - Posiçoes

E1=estrela(Point(450, 300), 211.7, color_rgb(255, 255, 0), color_rgb(255, 255, 0), win)



win.getMouse()
win.close()






UK



from graphics import *
from math import *

win = GraphWin("Flag of United Kingdom!", 1000, 500)
win.setBackground(color_rgb(0, 0, 139))




retangulo1 = Rectangle(Point(0, 500/3), Point(1000, 1000/3))
retangulo1.setFill(color_rgb(255, 255, 255))
retangulo1.setOutline(color_rgb(255, 255, 255))
retangulo1.draw(win)

retangulo2 = Rectangle(Point(1000*5/12, 0), Point(1000*7/12, 500))
retangulo2.setFill(color_rgb(255, 255, 255))
retangulo2.setOutline(color_rgb(255, 255, 255))
retangulo2.draw(win)

retangulo3 = Rectangle(Point(0, 500*2/5), Point(1000, 500*3/5))
retangulo3.setFill(color_rgb(255, 0, 0))
retangulo3.setOutline(color_rgb(255, 0, 0))
retangulo3.draw(win)

retangulo4 = Rectangle(Point(1000*9/20, 0), Point(1000*11/20, 500))
retangulo4.setFill(color_rgb(255, 0, 0))
retangulo4.setOutline(color_rgb(255, 0, 0))
retangulo4.draw(win)

def estrela(pontoM, lado, a, corFundo, Outline, janela):
   # ponto A
   pA = Point(pontoM.getX() + lado * cos(radians(a + 90)), pontoM.getY() - lado * sin(radians(a + 90)))

   # ponto B
   pB = Point(pontoM.getX() + lado * cos(radians(a + 162)), pontoM.getY() - lado * sin(radians(a + 162)))

   # ponto C
   pC = Point(pontoM.getX() + lado * cos(radians(a + 234)), pontoM.getY() - lado * sin(radians(a + 234)))

   # ponto D
   pD = Point(pontoM.getX() + lado * cos(radians(a + 306)), pontoM.getY() - lado * sin(radians(a + 306)))

   # ponto E
   pE = Point(pontoM.getX() + lado * cos(radians(a + 18)), pontoM.getY() - lado * sin(radians(a + 18)))

   p = Polygon(pA, pC, pE, pB, pD)
   p.setOutline(Outline)
   p.setFill(corFundo)
   p.draw(janela)


# Estrelas - Posiçoes

E1 = estrela(Point(150, 150), 5, 0, color_rgb(255, 255, 255), color_rgb(255, 255, 255), win)

win.getMouse()
win.close()



