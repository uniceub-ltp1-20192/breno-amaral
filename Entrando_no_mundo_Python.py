#Exercício-1.1
print("Já é meu segundo programa")
 
#Exercício-1.2
print("1 - cadastrar")
print("2 - logar")
print("3 - sair")
 
#Exercício-1.3
for a in range(2, 11, 2):
 print(a)

#Exercício-2.1
quantidade_cebolas = 7
preco_cebolas = 4
resultado = quantidade_cebolas*preco_cebolas
print("Você irá pagar", resultado, "pelas cebolas")
 
#Exercício-2.2
#atribui-se o inteiro à variável
cebolas = 300
#atribui-se o inteiro à variável
cebolas_na_caixa = 120
#atribui-se o inteiro à variável
espaco_caixa = 5
#atribui-se o inteiro à variável
caixas = 60
#atribui-se a operação à variável
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#atribui-se o inteiro à variável
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
#atribui-se o inteiro à variável
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
#imprime-se variável "cebolas_na_caixa", e a insere em um contexto
print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
#imprime-se variável "cebolas_fora_da_caixa", e a insere em um contexto
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
#imprime-se variável "espaco_caixa", e a insere em um contexto
print("Em cada caixa cabem", espaco_caixa, "cebolas")
#imprime-se variável "caixas_vazias", e a insere em um contexto
print("Ainda temos,", caixas_vazias, "caixas vazias")
#imprime-se variável "caixas_necessarias", e a insere em um contexto
print("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")

#Exercício-2.3
#atribui-se o inteiro à variável
cebolas = int(input())
#atribui-se o inteiro à variável
cebolas_na_caixa = int(input())
#atribui-se o inteiro à variável
espaco_caixa = int(input())
#atribui-se o inteiro à variável
caixas = int(input())
#atribui-se a operação à variável
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#atribui-se o inteiro à variável
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
#atribui-se o inteiro à variável
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
#imprime-se variável "cebolas_na_caixa", e a insere em um contexto
print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
#imprime-se variável "cebolas_fora_da_caixa", e a insere em um contexto
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
#imprime-se variável "espaco_caixa", e a insere em um contexto
print("Em cada caixa cabem", espaco_caixa, "cebolas")
#imprime-se variável "caixas_vazias", e a insere em um contexto
print("Ainda temos,", caixas_vazias, "caixas vazias")
#imprime-se variável "caixas_necessarias", e a insere em um contexto
print("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")
 
#Exercício-4.1
name = str(input("Digite um nome: "))
saida_a = name.lstrip()
saida_b = name.rstrip()
saida_c = name.strip()
print("\t", name)
print(saida_a,"\n", saida_b,"\n", saida_c)
 

#Exercício-4.2
name = str(input("Digite um nome: "))
r = name.replace(" ", "")
a = len(r)
 
print("A frase: ", r, "contém: ", a, "caracteres.")
 

#Exercício-5.1
nb = int(input("Digite um número: "))
if nb % 2 == 0:
 r = "par"
else:
 r = "ímpar"
 
print("O número digitado é", r +".")

#Exercício-5.2
nb = int(input("Digite um número: "))
d = nb - 1
if nb == 2:
 print("O número é primo")
while d > 1:
 r = nb % d
 if d == 2 and r != 0:
   print("O número é primo")
 if r != 0:
   d -= 1
 elif r == 0:
   d = 0
   print("O número não é primo")
 
 
 
#Exercício-5.3
nb_a = float(input("Digite um número: "))
nb_b = float(input("Digite um número: "))
a, b, c, d = nb_a + nb_b, nb_a - nb_b, nb_a * nb_b, nb_a / nb_b
 
print(a, b, c, d)
 
#Exercício-5.4
i = int(input("Informe a idade: "))
s = int(input("Informe o semestre: "))
 
r = (8 - s) / 2
print(r)
 
#Exercício-5.5
s = int(input("Informe o semestre cursado: "))
i = int(input("Informe quantos semestres estão atrasados: "))
 
r = (8 + i - s) / 2
print(r, "anos")
 
#Exercício-6.1
nomes = ["Arthur", "Maria", "João", "Ana", "Pedro"]
print(nomes[0], nomes[1], nomes[2], nomes[3], nomes[4])
 
#Exercício-6.2
nomes = ["Arthur", "Maria", "João", "Ana", "Pedro"]
print("Olá", nomes[0] + ", seja bem-vindo(a)!", "Olá", nomes[1] + ", seja bem-vindo(a)!", "Olá", nomes[2] + ", seja bem-vindo(a)!", "Olá", nomes[3] + ", seja bem-vindo(a)!", "Olá", nomes[4] + ", seja bem-vindo(a)!")
 
#Exercício-6.3
nomes = ["moto da Yamaha", "moto da Honda", "moto da Kawasaki", "carro da Aston Martin", "carro da Audi", "carro da Mercedes-Benz"]
 
for n in nomes:
 print("Eu gostaria de ter um(a)", n)
 
#Exercício-7.1
nomes = ["Samuel Rosa", "Nena", "Mujica", "Bob Marley", "Jimmy Page"]
 
for n in nomes:
 print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
 
#Exercício-7.2
nomes = ["Samuel Rosa", "Nena", "Mujica", "Bob Marley", "Jimmy Page"]
 
for n in nomes:
 print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
 
print("O convidado:", nomes[3], "não poderá comparecer ao evento.\n")
 
nomes[3] = "Uesley"
 
for n in nomes:
 print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
 
#Exercício-7.3
nomes = ["Samuel Rosa", "Nena", "José Pepe Mujica", "Bob Marley", "Jimmy Page"]
for n in nomes:
print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
print("O convidado:", nomes[3], "não poderá comparecer ao evento.\n")
nomes[3] = "Uesley"
for n in nomes:
print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
 
print("Caros convidados, encontramos uma mesa maior para o jantar.\n")
 
nomes.insert(0, "Mark Knopfler")
nomes.insert(3, "Jobson")
nomes.append("Abner")
for n in nomes:
print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
 
#Exercício-7.4
nomes = ["Samuel Rosa", "Nena", "José Pepe Mujica", "Bob Marley", "Jimmy Page"]
for n in nomes:
print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
print("O convidado:", nomes[3], "não poderá comparecer ao evento.\n")
nomes[3] = "Uesley"
for n in nomes:
print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
 
print("Caros convidados, encontramos uma mesa maior para o jantar.\n")
 
nomes.insert(0, "Mark Knopfler")
nomes.insert(3, "Jobson")
nomes.append("Abner")
for n in nomes:
print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
 
print(nomes)
print("Caro, " + nomes[3] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(3)
 
print("Caro, " + nomes[3] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(3)
 
print("Caro, " + nomes[1] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(1)
 
print("Caro, " + nomes[4] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(4)
 
print("Caro, " + nomes[3] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(3)
 
print("Caro, " + nomes[2] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(2)
 
for n in nomes:
print("Caro(a)", n, "tu ainda está convidado(a) para o jantar.\n")
 
del nomes[0]
del nomes[0]
 
print(nomes)
 
#Exercício-8.1
def func ():
 lugares = ["Myanmar", "México", "Egito", "Jamaica", "Alemanha"]
 print(lugares)
 print(sorted(lugares))
 print(lugares)
 print(sorted(lugares, reverse=True))
 print(lugares)
 lugares.reverse()
 print(lugares)
 lugares.reverse()
 print(lugares)
 lugares.sort()
 print(lugares)
 lugares.sort(reverse=True)
 print(lugares)
 
func()
 
#Exercício-8.2
nomes = ["Samuel Rosa", "Nena", "José Pepe Mujica", "Bob Marley", "Jimmy Page"]
print(len(nomes))
for n in nomes:
 print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
print("O convidado:", nomes[3], "não poderá comparecer ao evento.\n")
nomes[3] = "Uesley"
for n in nomes:
 print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
 print("Caros convidados, encontramos uma mesa maior para o jantar.\n")
nomes.insert(0, "Mark Knopfler")
nomes.insert(3, "Jobson")
nomes.append("Abner")
for n in nomes:
 print("Caro(a)", n, "tu está convidado(a) para jantar na casa... no dia... às...\n")
print(nomes)
print(len(nomes))
print("Caro, " + nomes[3] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(3)
print("Caro, " + nomes[3] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(3)
print("Caro, " + nomes[1] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(1)
print("Caro, " + nomes[4] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(4)
print("Caro, " + nomes[3] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(3)
print("Caro, " + nomes[2] + ". Não teremos como fornecer a janta para ti. Infelizemente tu estás desconvidado\n")
r_a = nomes.pop(2)
for n in nomes:
 print("Caro(a)", n, "tu ainda está convidado(a) para o jantar.\n")
 
print(len(nomes))
 
del nomes[0]
del nomes[0]
print(nomes)
print(len(nomes))
 
#Exercício-8.3
def func ():
lugares = ["Myanmar", "México", "Egito", "Jamaica", "Alemanha", "Islândia", "Grécia", "Camboja", "Uruguai", "Nova Zelândia"]
print(lugares)
lugares.reverse()
print(lugares)
print(sorted(lugares))
print(sorted(lugares, reverse=True))
lugares.sort()
print(lugares)
lugares.sort(reverse=True)
print(lugares)
print(len(lugares))
 
func()
 
#Exercício-9.1
def a():
  lista = [ 1, 2, 3, 4]
  n = sum(lista)
  print(n)
 
a()
 
#Exercício-9.2
def b():
 lista = [1, 2, 3, 4, 5]
 m = 1
 for n in lista:
   m = m*n
 print(m)
 
b()
 
 
#Exercício-9.3
def c():
  lista = [1, 2, 3, 4, 5]
  p = (min(lista))
  print(p)
 
c()
 
#Exercício-9.4
def b():
 lista_A = [1, 2, 3, 4, 5]
 lista_B = [7, 2, 3, 9, 0]
 lista_C = []
 for n in lista_A:
   if n in lista_B:
     lista_C.insert(0, n)
 return print(lista_C)
 
b()
 
#Exercício-9.5
def e():
  lista_A = ["a", "b", "d", "e", "c"]
  lista_A.sort()
  print(lista_A)
e()
 
#Exercício-9.6
def e():
  lista_A = ["abc", "bdsdsa", "sd", "sdsddddde", "saasasasac"]
  n = 0
  while n < len(lista_A):
	a = len(lista_A[n])
	print(a)
	n += 1
 
e()
 
#Exercício-9.7
def b():
 lista = ["oi", "ola", "boa", "bem", "obrg", "aol", "ola"]
 lis = []
 p = "lao"
 for n in lista:
   if sorted(n) == sorted(p):
     lis.insert(0, n)
 print(lis)
 
b()
 
#Exercício-10.1
def funcao():
 for n in range(1, 21):
   print(n)
 
funcao()
 
#Exercício-10.2
def funcao():
lista = list(range(1, 1000001))
print(lista)
 
funcao()
 
#Exercício-10.3
def funcao():
 lista = list(range(1, 1000001))
 print(min(lista))
 print(max(lista))
 print(sum(lista))
 
funcao()
 
#Exercício-10.4
def funcao():
  lista = list(range(1, 20, 2))
  for z in lista:
  print(z)
 
funcao()
 
#Exercício-10.5
def funcao():
lista = list(range(0, 1000, 3))
  for z in lista:
  print(z)
 
funcao()
 
#Exercício-10.6
def funcao():
 lista = []
 for n in range(1, 101):
   r = n**3
   lista.append(r)
 for z in lista:
  print(z)
 
 funcao()
 
#Exercício-10.7
def funcao():
lista = []
for n in range(1, 101):
  lista.append(n**3)
 for z in lista:
  print(z)
 
 funcao()
 
 
 
funcao()
 
#Exercício-11.1
def func ():
lugares = ["Myanmar", "México", "Egito", "Jamaica", "Alemanha"]
print("Os primeiros 3 itens da lista são: ", lugares[ :3])
print("Os últimos 3 itens da lista são: ",lugares[2: ])
print("Os 3 itens do meio da lista são: ",lugares[1:4])
 
func()
 
#Exercício-11.2
def func ():
pizza_hut = ["calabresa", "mussarela", "portuguesa", "frango", "chocolate"]
pizza_hot_paraguai = pizza_hut[ : ]
pizza_hut.append("banana")
pizza_hot_paraguai.append("lombo canadense")
print(pizza_hut)
print(pizza_hot_paraguai)
 
func()
 
#Exercíco-11.3
def funcao():
 lugares = ["Myanmar", "México", "Egito", "Jamaica", "Alemanha"]
 for z in lugares:
  print(z)
 
 funcao()
 
#Exercício-12.4
def func_A():
 buffet = ("massa", "batata frita", "bife", "sorvete", "brigadeiro")
 for n in buffet:
   print(n)
def func_B():
 buffet[2] = "camarão"
def func_C():
 buffet = ("peixe", "batata frita", "bife", "frutas", "brigadeiro")
 for m in buffet:
   print(m)
 
func_A()
func_C()
func_B()
 
 
 
 
